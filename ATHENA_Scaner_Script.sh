#!/bin/bash


echo "Start Scanning"
tool=lighter # you can change to Racer or Blue
model=0 # 0 is for N-gram, 1 is for RNN
DatasetPath="" # set the path and rename data set file to Data.fa
SRILMPath=""
RNNPath=""
ReadLength=36 # change to the correct read length
Coverage=60 # change to the correct coverage
MaxNumOfRuns=1000
NumOfReads=10000 # change to correct num of reads in dataset

K_0=$((ReadLength / 2))
GenomeLength=3000000 # change to correct genome length

echo "K_0 is $K_0"

if [ $model == 0 ]; then
	$SRILMPath/ngram-count -order 3 -text $DatasetPath/Data.fa -lm Data_ngram.lm
elif [ $model == 1 ]; then
	python train.py --data_file=$DatasetPath/Data.fa --init_dir=output-folder
fi


python train.py --data_file=input-data-file --init_dir=output-folder

for s in {1..$MaxNumOfRuns}; do

	if [ $Coverage -gt 30 ]; then
		SampleSize=$((30/Coverage * NumOfReads))
	
		echo "SampleSize is $SampleSize"
	        java My_Sampler $DatasetPath/Data.fa  SampleSize $DatasetPath/Data_sample.fa 
	else
		cp $DatasetPath/Data.fa $DatasetPath/Data_sample.fa # use all dataset, no sampling
	fi
	
	# perform correction
	corrected_data_path=""
	if [ $tool == "lighter" ]; then
		lighter -r $DatasetPath/Data_sample.fa -K $K_0 5000000 -t 10 -od Lighter_Output/ 	
		corrected_data_path=Lighter_Output/Data_sample.fa.corrected
	elif  [ $tool == "blue" ]; then
		mono Tessel.exe -k $K_0 -m 50 -t 4 $DatasetPath/Data_sample.fa corrected_data.fasta
		mono Blue.exe -m 50 -t 4 $DatasetPath/Data_sample.fa_$K_0.cbt corrected_data.fasta
		corrected_data_path=corrected_data.fasta
	elif [ $tool == "racer" ]; then
		./RACER $DatasetPath/Data_sample.fa $DatasetPath/Data_sample_corrected_racer.fa $GenomeLength
		corrected_data_path=$DatasetPath/Data_sample_correcter_racer.fa
	fi
	
	
	# extimate ppl
	if [ $model == 0 ]; then
		java -cp fasta-utils-1.1.ja:. My_Fasta_Reader $corrected_data_path 7 $corrected_data_path'.splitted'	
		./ngram -order 3 -lm Data_ngram.lm -ppl $corrected_data_path'.splitted';
	elif [ $model == 1 ]; then
		./get_line_by_line_perplexity $corrected_data_path
	fi

	for L in {1..4} ; do
		
		GL=$(( $L * 1500000 ))
		echo "correcting sample $s with GL= $GL"
		
		cd Dataset
		java My_Sampler	D2/SRR022918.fa 1000000 D2/SRR022918.sample.fa
	

		D1/Lighter/Lighter/lighter -r D2/SRR022918.sample_$1.fa -K $k 7000000 -t 10 -od D2/corrected_sample
		cd Dataset/D1/seqan-seqan-v2.4.0/racer

		./Racer /home/amahgoub/Desktop/Project_FTC/Dataset/D2/SRR022918.sample_$1.fa /home/amahgoub/Desktop/Project_FTC/Dataset/D2/SRR022918.sample_$1.racer.cor.fa $GL

		cd /home/amahgoub/Desktop/Project_FTC
	
		./bowtie2-2.3.3.1/bowtie2 -x bowtie2-2.3.3.1/D1_Index/D1 -f Dataset/D2/SRR022918.sample_$1.fa -S eg1_$1.before.sam
		./bowtie2-2.3.3.1/bowtie2 -x bowtie2-2.3.3.1/D1_Index/D1 -f Dataset/D2/SRR022918.sample_$1.racer.cor.fa -S eg1_$1.after.sam

		./Dataset/D1/seqan-seqan-v2.4.0/FIONA/bin/compute_gain -g Dataset/D1/Reference_Genome/NC_00913_sequence.fasta --pre eg1_$1.before.sam --post-sam eg1_$1.after.sam -q
		

		cd Dataset
		java -cp fasta-utils-1.1.ja:. My_Fasta_Reader D2/SRR022918.sample_$1.racer.cor.fa 7 D2/SRR022918.sample_$1.racer.cor.splitted.fa		
		cd /home/amahgoub/Desktop/Project_FTC
		
		cd srilm-1.7.2/bin/i686-m64/
		./ngram -order 3 -lm D2.lm -ppl ../../../Dataset/D2/SRR022918.sample_$1.racer.cor.splitted.fa		
		
		cd /home/amahgoub/Desktop/Project_FTC

		echo "============================="
		echo "============================="

	done
done



