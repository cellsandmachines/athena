All the following tools has to be installed before using ATHENA:
=====================================

1- EC tool (e.g. Lighter, Blue, Racer)

---> Lighter: https://github.com/mourisl/Lighter
---> Blue: https://github.com/PaulGreenfieldOz/WorkingDogs
---> Racer: https://github.com/lucian-ilie/RACER

2- N-gram Language Model training library (e.g. SRILM)
---> SRILM: https://github.com/MicrosoftLearning/Speech-Recognition/tree/master/M4_Language_Modeling/srilm/bin/cygwin64

3- Char RNN Language Model training library (e.g. Char-RNN-Tensorflow)
--->https://github.com/sherjilozair/char-rnn-tensorflow

4- FASTA format preprocessor
---> My_Fasta_Reader.java (included)

5- Sampler
---> My_Sampler.java (included)

6= Char RNN PPL estimator
---> get_line_by_line_perplexity (included)