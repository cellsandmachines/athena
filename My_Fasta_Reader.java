//package org.yeastrc.fasta;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import org.yeastrc.fasta.*;

/**
 * Very simple example for processing a FASTA file.
 *
 */
public class My_Fasta_Reader {

    /**
     * Iterate over the supplied FASTA filename and print simple information to
     * console
     * 
     * @param filename
     * @throws Exception
     */
    public static String splitEqually(String text, int size) 
    {
    // Give the list the right capacity to start with. You could use an array
    // instead if you wanted.
	    StringBuilder sb = new StringBuilder();
	    List<String> ret = new ArrayList<String>((text.length() + size - 1) / size);

	    for (int start = 0; start < text.length(); start += size) {
	        //ret.add(text.substring(start, Math.min(text.length(), start + size)));
		sb.append(text.substring(start, Math.min(text.length(), start + size))).append(" ");
	    }
	    return sb.toString();
    }
    private void processFASTAFile( String filename , String K_str , String OutFile) throws Exception {

	int K_int = Integer.parseInt(K_str);

        // Instantiate a reader for the file with the supplied filename
	//FASTAReader reader = FASTAReader.getInstance( filename );
	File input_file = new File(filename);
	File file = new File(OutFile);
	file.createNewFile();
        FileWriter writer = new FileWriter(file); 
        Scanner input = new Scanner(input_file);


       int counter = 1;
       while (input.hasNextLine())
       {
	




            /*
             *  A given FASTA entry may have multiple headers associated with a
             *  sequence. Show them all.
             */
            //for( FASTAHeader header : entry.getHeaders() ) {
                //System.out.println( "Got a FASTA header with name=" +
                 //   header.getName() + " and description=" + header.getDescription() );
            //}

            // show the sequence for this FASTA entry
	    //StringBuilder sb = new StringBuilder();
	    String line = input.nextLine();
	    //line = input.nextLine();
	    if(line.contains(">"))
		continue;
	    String chunks = splitEqually(line,K_int);

            //System.out.println( "--> " + chunks + "\n" );
            writer.write(chunks + "\n");



		
//	    writer.write("This\n is\n an\n example\n"); 

	    writer.flush();
           
	    //writer.close();
	    
            // get the next entry in the FASTA file
            //entry = reader.readNext();
        }
	input.close();
	writer.close();

    }


    /**
     * Run program, supply full path to FASTA file as first argument
     * @param args
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {

        My_Fasta_Reader example = new My_Fasta_Reader();
        example.processFASTAFile( args[ 0 ] , args[ 1 ], args[ 2 ]);

    }

}
